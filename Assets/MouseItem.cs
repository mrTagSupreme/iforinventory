﻿using UnityEngine;
using System.Collections;

public class MouseItem : MonoBehaviour {

	public Item draggedItem;
	public UnityEngine.UI.RawImage itemTexture;
	public UnityEngine.UI.RawImage itemBG;
	public int itemIsFromSlotX;
	public int itemIsFromSlotY;
	public EquipmentSlots itemIsFromEquipmentSlot;
	public int itemIsFromQuickSlot;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setItem(Item myItem) {
		if (myItem == null) {
			draggedItem = null;
		} else {
			draggedItem = myItem;
			itemTexture.texture = myItem.ItemTexture;
		}
	}




}
