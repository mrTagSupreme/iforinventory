﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Flags]
public enum EquipmentSlots {
    None            = 1 << 0,
    HandLefSlot     = 1 << 1,
    HandRightSlot   = 1 << 2,
    ShoeLeftSlot    = 1 << 3,
    ShoeRightSlot   = 1 << 4,
    PantsSlot       = 1 << 5,
    BodyArmorSlot   = 1 << 6,
    HeadSlot        = 1 << 7,
    NecklaceSlot    = 1 << 8,
    TrinketSlot     = 1 << 9,
    QuickSlot       = 1 << 10
}

public class Character : MonoBehaviour {

    public BaseStats Stats;
    public int Gold;

    private const int InventorySizeX = 6;
    private const int InventorySizeY = 3;
    private Item[,] Inventory;
    class InventoryCoord : IEquatable<InventoryCoord>
    {
        public InventoryCoord(int x, int y) { X = x; Y = y; }
        public int X, Y;
        public bool Equals(InventoryCoord c2) { return X == c2.X && Y == c2.Y;  }
    }
    private List<InventoryCoord> FreeInventorySlots = new List<InventoryCoord>();

    private EquipmentSlots FreeEquipmentSlots;

    private const int QuickSlotsSize = 4;
    private Item[] QuickSlots;
    private List<int> FreeQuickSlots = new List<int>();

    private Item HandLeft;
    private Item HandRight;
    private Item ShoeLeft;
    private Item ShoeRight;
    private Item Pants;
    private Item BodyArmor;
    private Item Head;
    private Item Necklace;
    private Item Trinket;

    public delegate void SlotsChangedDelegate(Character c);
    public SlotsChangedDelegate OnSlotsChanged;

	// Use this for initialization
	public Character () {
	    Inventory = new Item[InventorySizeX,InventorySizeY];
        QuickSlots = new Item[QuickSlotsSize];
        for (int inventoryX = 0; inventoryX < InventorySizeX; ++inventoryX)
        {
            for (int inventoryY = 0; inventoryY < InventorySizeY; ++inventoryY)
            {
                FreeInventorySlots.Add(new InventoryCoord(inventoryX, inventoryY));
            }
        }
        for (int quickSlotIndex = 0; quickSlotIndex < QuickSlotsSize; ++quickSlotIndex)
        {
            FreeQuickSlots.Add(quickSlotIndex);
        }

        FreeEquipmentSlots = EquipmentSlots.BodyArmorSlot | EquipmentSlots.HandLefSlot | EquipmentSlots.HandRightSlot |
            EquipmentSlots.HeadSlot | EquipmentSlots.NecklaceSlot | EquipmentSlots.PantsSlot | EquipmentSlots.ShoeLeftSlot |
            EquipmentSlots.ShoeRightSlot | EquipmentSlots.TrinketSlot;
	}

    public void tick()
    {
        if (Stats.Health / Stats.MaxHealth < 0.5f && FreeQuickSlots.Count < 4)
        {
            List<int> quickSlotItems = new List<int>();
            for (int quickslotIndex = 0; quickslotIndex < QuickSlots.Length; ++quickslotIndex)
            {
                if (QuickSlots[quickslotIndex] != null)
                {
                    quickSlotItems.Add(quickslotIndex);
                }
            }
            int randomQuickSlotIndex = quickSlotItems[UnityEngine.Random.Range(0, quickSlotItems.Count)];
            Item quickslotItem = getItemFromQuickSlotAndSetNull(randomQuickSlotIndex);
            quickslotItem.consume(Stats);
            GameMaster.getInstance().Log.AddToLog("The Hero consumed " + quickslotItem.Name);
            Destroy(quickslotItem);
        }

        Stats.tick();
    }

    public Item getEquippedItem(EquipmentSlots slot)
    {
        switch (slot)
        {
            case EquipmentSlots.BodyArmorSlot: return BodyArmor;
            case EquipmentSlots.HandLefSlot: return HandLeft;
            case EquipmentSlots.HandRightSlot: return HandRight;
            case EquipmentSlots.HeadSlot: return Head;
            case EquipmentSlots.NecklaceSlot: return Necklace;
            case EquipmentSlots.PantsSlot: return Pants;
            case EquipmentSlots.ShoeLeftSlot: return ShoeLeft;
            case EquipmentSlots.ShoeRightSlot: return ShoeRight;
            case EquipmentSlots.TrinketSlot: return Trinket;
        }
        return null;
    }

    public bool isEquipmentSlotUsed(EquipmentSlots slot)
    {
        return (slot & FreeEquipmentSlots) != slot;
    }

    public void setEquippedItem(EquipmentSlots slot, Item item)
    {
        Item previousItem = null;
        switch (slot)
        {
            case EquipmentSlots.BodyArmorSlot: previousItem = BodyArmor; BodyArmor = item; break;
            case EquipmentSlots.HandLefSlot: previousItem = HandLeft; HandLeft = item; break;
            case EquipmentSlots.HandRightSlot: previousItem = HandRight; HandRight = item; break;
            case EquipmentSlots.HeadSlot: previousItem = Head; Head = item; break;
            case EquipmentSlots.NecklaceSlot: previousItem = Necklace; Necklace = item; break;
            case EquipmentSlots.PantsSlot: previousItem = Pants; Pants = item; break;
            case EquipmentSlots.ShoeLeftSlot: previousItem = ShoeLeft; ShoeLeft = item; break;
            case EquipmentSlots.ShoeRightSlot: previousItem = ShoeRight; ShoeRight = item; break;
            case EquipmentSlots.TrinketSlot: previousItem = Trinket; Trinket = item; break;
        }
        if (previousItem)
        {
            Stats.removeItem(previousItem);
        }
        if (item)
        {
            Stats.equipItem(item);
            item.transform.parent = transform;
        }
        if (OnSlotsChanged != null) OnSlotsChanged(this);
    }

    public bool putItemIntoNextFreeSlot(Item item)
    {
        EquipmentSlots sharedSlots = item.AllowedSlots & FreeEquipmentSlots;
        if (sharedSlots != EquipmentSlots.None)
        {
            if ((sharedSlots & EquipmentSlots.QuickSlot) == EquipmentSlots.QuickSlot)
            {
                return putItemIntoRandomQuickSlot(item);
            }
            else if ((sharedSlots & EquipmentSlots.BodyArmorSlot) == EquipmentSlots.BodyArmorSlot)
            {
                setEquippedItem(EquipmentSlots.BodyArmorSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.HandLefSlot) == EquipmentSlots.HandLefSlot)
            {
                setEquippedItem(EquipmentSlots.HandLefSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.HandRightSlot) == EquipmentSlots.HandRightSlot)
            {
                setEquippedItem(EquipmentSlots.HandRightSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.HeadSlot) == EquipmentSlots.HeadSlot)
            {
                setEquippedItem(EquipmentSlots.HeadSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.NecklaceSlot) == EquipmentSlots.NecklaceSlot)
            {
                setEquippedItem(EquipmentSlots.NecklaceSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.PantsSlot) == EquipmentSlots.PantsSlot)
            {
                setEquippedItem(EquipmentSlots.PantsSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.ShoeLeftSlot) == EquipmentSlots.ShoeLeftSlot)
            {
                setEquippedItem(EquipmentSlots.ShoeLeftSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.ShoeRightSlot) == EquipmentSlots.ShoeRightSlot)
            {
                setEquippedItem(EquipmentSlots.ShoeRightSlot, item);
                return true;
            }
            else if ((sharedSlots & EquipmentSlots.TrinketSlot) == EquipmentSlots.TrinketSlot)
            {
                setEquippedItem(EquipmentSlots.TrinketSlot, item);
                return true;
            }
        }
        return false;
    }

    public Item getInventoryItem(int inventoryX, int inventoryY)
    {
        return Inventory[inventoryX, inventoryY];
    }

    public void setInventoryItem(int inventoryX, int inventoryY, Item item)
    {
        Inventory[inventoryX, inventoryY] = item;
        if(item != null){
            FreeInventorySlots.Remove(new InventoryCoord(inventoryX, inventoryY));
            item.transform.parent = transform;
        }
        else {
            FreeInventorySlots.Add(new InventoryCoord(inventoryX, inventoryY));
        }
        if (OnSlotsChanged != null) OnSlotsChanged(this);
    }

    public Item getQuickSlotItem(int quickslotIndex)
    {
        return QuickSlots[quickslotIndex];
    }

    public void setQuickSlotItem(int quickslotIndex, Item item)
    {
        QuickSlots[quickslotIndex] = item;
        if(item != null){
            FreeQuickSlots.Remove(quickslotIndex);
            item.transform.parent = transform;
        }
        else {
            FreeQuickSlots.Add(quickslotIndex);
        }
        if (OnSlotsChanged != null) OnSlotsChanged(this);
    }

    public void putObjectFromInventoryToEquipment(int inventoryX, int inventoryY, EquipmentSlots slot)
    {
        Item item = getInventoryItem(inventoryX, inventoryY);
        if (item)
        {
            setInventoryItem(inventoryX, inventoryY, null);
            setEquippedItem(slot, item);
        }
    }

    public void putObjectFromEquipementToInventory(EquipmentSlots slot, int inventoryX, int inventoryY)
    {
        Item item = getEquippedItem(slot);
        setInventoryItem(inventoryX, inventoryY, item);
        setEquippedItem(slot, null);
    }

    public Item getItemFromInventoryAndSetNull(int inventoryX, int inventoryY)
    {
        Item item = getInventoryItem(inventoryX, inventoryY);
        setInventoryItem(inventoryX, inventoryY, null);
        return item;
    }

    public Item getItemFromEquipmentAndSetNull(EquipmentSlots slot)
    {
        Item item = getEquippedItem(slot);
        setEquippedItem(slot, null);
        return item;
    }

    public Item getItemFromQuickSlotAndSetNull(int quickslotIndex)
    {
        Item item = getQuickSlotItem(quickslotIndex);
        setQuickSlotItem(quickslotIndex, null);
        return item;
    }

    public bool hasFreeInventorySlots()
    {
        return FreeInventorySlots.Count > 0;
    }

    public bool putItemIntoRandomInventorySlot(Item item)
    {
        if (FreeInventorySlots.Count == 0)
            return false;
        int randomFreeSlotIndex = UnityEngine.Random.Range(0, FreeInventorySlots.Count);
        setInventoryItem(FreeInventorySlots[randomFreeSlotIndex].X, FreeInventorySlots[randomFreeSlotIndex].Y, item);
        return true;
    }

    public bool putItemIntoRandomQuickSlot(Item item)
    {
        if (FreeQuickSlots.Count == 0) 
            return false;
        int randomFreeQuickSlotIndex = UnityEngine.Random.Range(0, FreeQuickSlots.Count);
        setQuickSlotItem(FreeQuickSlots[randomFreeQuickSlotIndex], item);
        return true;
    }
}
