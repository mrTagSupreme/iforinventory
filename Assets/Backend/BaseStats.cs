﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DamageTypes
{
    Physical,
    Poison,
    Fire,
    Ice,
    Lightning
}

public enum BaseAttributes
{
    StrengthAtt,
    AgilityAtt,
    IntelligenceAtt,
    ConstitutionAtt,
    HealthAtt,
    ManaAtt,
    MaxHealthAtt,
    MaxManaAtt,
    ArmorAtt,
    PoisonResAtt,
    FireResAtt,
    IceResAtt,
    LightningResAtt
}

public class BaseStats : MonoBehaviour
{
    static public string getDamageTypeString(DamageTypes damageType)
    {
        switch (damageType)
        {
            case DamageTypes.Physical: return "physical";
            case DamageTypes.Poison: return "poison";
            case DamageTypes.Fire: return "fire";
            case DamageTypes.Ice: return "ice";
            case DamageTypes.Lightning: return "lightning";
        }
        return "";
    }

    static public string getAttributeTypeString(BaseAttributes attribute)
    {
        switch (attribute)
        {
            case BaseAttributes.AgilityAtt: return "Agility";
            case BaseAttributes.ArmorAtt: return "Armor";
            case BaseAttributes.ConstitutionAtt: return "Constitution";
            case BaseAttributes.FireResAtt: return "Fire resist";
            case BaseAttributes.HealthAtt: return "Health";
            case BaseAttributes.IceResAtt: return "Ice resist";
            case BaseAttributes.IntelligenceAtt: return "Intelligence";
            case BaseAttributes.LightningResAtt: return "Lightning resist";
            case BaseAttributes.ManaAtt: return "Mana";
            case BaseAttributes.MaxHealthAtt: return "Max Health";
            case BaseAttributes.MaxManaAtt: return "Max Mana";
            case BaseAttributes.PoisonResAtt: return "Poison resist";
            case BaseAttributes.StrengthAtt: return "Strength";
        }
        return "";
    }

    static public float getLevelFactor(int level)
    {
        return 1 + 0.5f * level;
    }

    static public float getXPForLevel(int level)
    {
        float levelfactor = getLevelFactor(level);
        return 100.0f + 200.0f * levelfactor * levelfactor;
    }

    public string Name;

    [HideInInspector]
    public float Health, Mana, XP;
    [HideInInspector]
    public float Strength, Agility, Intelligence, Constitution;

    [HideInInspector]
    public int Level;
    public int StrengthPoints, AgilityPoints, IntelligencePoints, ConstitutionPoints;
    [HideInInspector]
    public float MaxHealth, MaxMana;
    public float AdditionalMaxHealth, AdditionalMaxMana;

    [HideInInspector]
    public float Armor, PoisonResistance, FireResistance, IceResistance, LightningResistance;
    public float BaseArmor, BasePoisonRes, BaseFireRes, BaseIceRes, BaseLightingRes;

    [HideInInspector]
    public float DodgeChance, BlockChance, CriticalHitChance;

    private List<BaseStatsModifier> Modifiers = new List<BaseStatsModifier>();
    private BaseStatsModifierCollection ModifierCollection = new BaseStatsModifierCollection();
    private bool StatsDirty = true;

    private float NextLevelXP, PreviousLevelXP;

    [HideInInspector]
    public List<Weapon> Weapons = new List<Weapon>();

    public delegate void BaseStatsChangedDelegate(BaseStats stats);
    public BaseStatsChangedDelegate OnBaseStatsChanged;

    public void clampHealthMana()
    {
        Health = Mathf.Clamp(Health, 0.0f, MaxHealth);
        Mana = Mathf.Clamp(Mana, 0.0f, MaxMana);
    }

    public void equipItem(Item item)
    {
        BaseStatsModifier[] modifiers = item.GetComponentsInChildren<BaseStatsModifier>();
        foreach (BaseStatsModifier mod in modifiers)
        {
            addModifier(mod);
        }
        Weapon[] weapons = item.GetComponentsInChildren<Weapon>();
        foreach (Weapon weap in weapons)
        {
            Weapons.Add(weap);
        }
    }

    public void removeItem(Item item)
    {
        BaseStatsModifier[] modifiers = item.GetComponentsInChildren<BaseStatsModifier>();
        foreach (BaseStatsModifier mod in modifiers)
        {
            removeModifier(mod);
        }
        Weapon[] weapons = item.GetComponentsInChildren<Weapon>();
        foreach (Weapon weap in weapons)
        {
            Weapons.Remove(weap);
        }
    }

    public void addModifier(BaseStatsModifier mod)
    {
        Modifiers.Add(mod);
        MarkStatsAsDirty();
    }

    public void removeModifier(BaseStatsModifier mod)
    {
        Modifiers.Remove(mod);
        MarkStatsAsDirty();
    }

    public void MarkStatsAsDirty()
    {
        StatsDirty = true;
    }

    void UpdateStats()
    {
        Strength = StrengthPoints;
        Agility = AgilityPoints;
        Intelligence = IntelligencePoints;
        Constitution = ConstitutionPoints;
        Armor = BaseArmor;
        PoisonResistance = BasePoisonRes;
        FireResistance = BaseFireRes;
        IceResistance = BaseIceRes;
        LightningResistance = BaseLightingRes;

        
        ModifierCollection.clear();
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.addToModifier(ModifierCollection);
        }
        
        ModifierCollection.applyBaseModification(this);

        MaxHealth = Constitution * 20 + AdditionalMaxHealth;
        MaxMana = Intelligence * 5 + AdditionalMaxMana;
        DodgeChance = Agility * 0.6f / 100.0f;
        CriticalHitChance = 0;
        BlockChance = 0;

        ModifierCollection.applyDependentModifications(this);

        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.applySpecialEffects(this);
        }

        clampHealthMana();

        StatsDirty = false;
        if (OnBaseStatsChanged != null)
        {
            OnBaseStatsChanged(this);
        }
    }

    public void tick()
    {
        if (StatsDirty)
        {
            UpdateStats();
        }

        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.tick(this);
        }

        Health += ModifierCollection.HealthPerTick;
        Mana += ModifierCollection.ManaPerTick;
        Mana += Intelligence * 0.1f;

        foreach (Weapon weap in Weapons)
        {
            weap.tick(this);
        }

        clampHealthMana();
    }


    public void gotAttackedByEnemy(BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed)
    {
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.gotAttackedByEnemy(this, enemyStats, damage, damageType, critical, blocked, missed);
        }

        foreach (Weapon weap in Weapons)
        {
            weap.gotAttackedByEnemy(this, enemyStats, damage, damageType, critical, blocked, missed);
        }
    }

    public void attackedEnemy(BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed)
    {
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.attackedEnemy(this, enemyStats, damage, damageType, critical, blocked, missed);
        }

        foreach (Weapon weap in Weapons)
        {
            weap.attackedEnemy(this, enemyStats, damage, damageType, critical, blocked, missed);
        }
    }

    public void killedEnemy(BaseStats enemyStats) {
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.killedEnemy(this, enemyStats);
        }

        foreach (Weapon weap in Weapons)
        {
            weap.killedEnemy(this, enemyStats);
        }
    }

    public float getResistance(DamageTypes damageType)
    {
        switch (damageType)
        {
            case DamageTypes.Fire: return FireResistance;
            case DamageTypes.Ice: return IceResistance;
            case DamageTypes.Lightning: return LightningResistance;
            case DamageTypes.Physical: return Armor;
            case DamageTypes.Poison: return PoisonResistance;
        }
        return 0;
    }

    public float getAttribute(BaseAttributes att)
    {
        switch (att)
        {
            case BaseAttributes.AgilityAtt: return Agility;
            case BaseAttributes.ArmorAtt: return Armor;
            case BaseAttributes.ConstitutionAtt: return Constitution;
            case BaseAttributes.FireResAtt: return FireResistance;
            case BaseAttributes.HealthAtt: return Health;
            case BaseAttributes.IceResAtt: return IceResistance;
            case BaseAttributes.IntelligenceAtt: return Intelligence;
            case BaseAttributes.LightningResAtt: return LightningResistance;
            case BaseAttributes.ManaAtt: return Mana;
            case BaseAttributes.MaxHealthAtt: return MaxHealth;
            case BaseAttributes.MaxManaAtt: return MaxMana;
            case BaseAttributes.PoisonResAtt: return PoisonResistance;
            case BaseAttributes.StrengthAtt: return Strength;
        }
        return 0;
    }

    public void addRandomAttributePoints(int count)
    {
        while (count > 0)
        {
            int randomAttribute = Random.Range(0, 4);
            switch (randomAttribute)
            {
                case 0: StrengthPoints += 1; break;
                case 1: AgilityPoints += 1; break;
                case 2: IntelligencePoints += 1; break;
                case 3: ConstitutionPoints += 1; break;
            }
            --count;
        }
        MarkStatsAsDirty();
    }

    public void addXP(float xp, bool displayLevelUpMessage = false)
    {
        XP += xp;
        while (XP >= NextLevelXP)
        {
            Level += 1;
            PreviousLevelXP = NextLevelXP;
            NextLevelXP = getXPForLevel(Level + 1);
            addRandomAttributePoints(5);
            if (displayLevelUpMessage)
            {
                GameMaster.getInstance().Log.AddToLog(Name + " gained a Level!");
            }
        }
    }

    public float getXPProgress()
    {
        return (XP - PreviousLevelXP) / (NextLevelXP - PreviousLevelXP);
    }

    public void addMana(float manaAdd)
    {
        Mana += manaAdd;
        clampHealthMana();
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.basicStatsChanged(this);
        }

        foreach (Weapon weap in Weapons)
        {
            weap.basicStatsChanged(this);
        }
    }

    public void addHealth(float healthAdd)
    {
        Health += healthAdd;
        clampHealthMana();
        foreach (BaseStatsModifier mod in Modifiers)
        {
            mod.basicStatsChanged(this);
        }

        foreach (Weapon weap in Weapons)
        {
            weap.basicStatsChanged(this);
        }
    }
}
