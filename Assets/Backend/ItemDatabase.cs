﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {

    public List<GameObject> StartItems = new List<GameObject>();
    public Item getRandomStartItem(BaseStats heroStats)
    {
        GameObject itemObject = GameObject.Instantiate(StartItems[Random.Range(0, StartItems.Count)]) as GameObject;
        Item item = itemObject.GetComponent<Item>();
        item.init(heroStats.Level);

        return item;
    }

    public List<GameObject> LootItems = new List<GameObject>();

}
