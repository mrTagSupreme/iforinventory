﻿using UnityEngine;
using System.Collections;

public class Consumable : MonoBehaviour {

    public float AddHealth, AddMana, AddXP, AddMaxHealth, AddMaxMana;
    public int AddStrength, AddAgility, AddIntelligence, AddConstitution;


    public virtual string getDescription()
    {
        string description = "";

        string modFormatString = "+#;-#;0";

        if (AddHealth != 0)
        {
            description += "Health " + AddHealth.ToString(modFormatString) + "\n";
        }
        if (AddMana != 0)
        {
            description += "Mana " + AddMana.ToString(modFormatString) + "\n";
        }
        if (AddXP != 0)
        {
            description += "XP " + AddXP.ToString(modFormatString) + "\n";
        }
        if (AddMaxHealth != 0)
        {
            description += "Max Health " + AddMaxHealth.ToString(modFormatString) + "\n";
        }
        if (AddMaxMana != 0)
        {
            description += "Max Mana " + AddMaxMana.ToString(modFormatString) + "\n";
        }
        if (AddStrength != 0)
        {
            description += "Strength " + AddStrength.ToString(modFormatString) + "\n";
        }
        if (AddAgility != 0)
        {
            description += "Agility " + AddAgility.ToString(modFormatString) + "\n";
        }
        if (AddConstitution != 0)
        {
            description += "Constitution " + AddConstitution.ToString(modFormatString) + "\n";
        }
        if (AddIntelligence != 0)
        {
            description += "Intelligence " + AddIntelligence.ToString(modFormatString) + "\n";
        }

        return description;
    }

    public virtual void modifyForLevel(int level) { }

    public virtual void consume(BaseStats stats)
    {
        stats.addHealth(AddHealth);
        stats.addMana(AddMana);
        stats.addXP(AddXP, true);
        stats.AdditionalMaxHealth += AddMaxHealth;
        stats.AdditionalMaxMana += AddMaxMana;
        stats.StrengthPoints += AddStrength;
        stats.AgilityPoints += AddAgility;
        stats.IntelligencePoints += AddIntelligence;
        stats.ConstitutionPoints += AddConstitution;

        stats.MarkStatsAsDirty();
    }
}
