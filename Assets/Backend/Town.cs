﻿using UnityEngine;
using System.Collections;

public enum TownState
{
    InTaverna,
    InShop,
    AtPriest,
    ReadyForDungeon
}

public class Town : MonoBehaviour
{
    private const int TicksPerSellingItem = 8;
    private int RemainingTicksForCurrentItem;
    private int CurrentSellingSlotNumber;

    private const float HealingPerTick = 5;

    public TownState CurrentState { get; private set; }

    public void enterTown(Character playerChar)
    {
        GameMaster.getInstance().Log.AddToLog("\nThe Hero visits the town...");
        // need healing?
        if (playerChar.Stats.Health < playerChar.Stats.MaxHealth)
        {
            CurrentState = TownState.AtPriest;
            GameMaster.getInstance().Log.AddToLog("The Hero visits the priest for some healing");
        }
        else
        {
            CurrentState = TownState.InShop;
            RemainingTicksForCurrentItem = TicksPerSellingItem;
            CurrentSellingSlotNumber = 0;
            GameMaster.getInstance().Log.AddToLog("The Hero prepares to sell his stuff.");
        }
    }

    public void tick(Character playerChar)
    {
        switch(CurrentState) {
            case TownState.AtPriest:
                playerChar.Stats.addHealth(HealingPerTick * BaseStats.getLevelFactor(playerChar.Stats.Level));
                if (playerChar.Stats.Health >= playerChar.Stats.MaxHealth)
                {
                    CurrentState = TownState.InShop;
                    RemainingTicksForCurrentItem = TicksPerSellingItem;
                    CurrentSellingSlotNumber = 0;
                    GameMaster.getInstance().Log.AddToLog("The Hero prepares to sell his stuff.");
                }
                break;
            case TownState.InShop:
                RemainingTicksForCurrentItem -= 1;
                if (RemainingTicksForCurrentItem <= 0)
                {
                    int currentInventoryX, currentInventoryY;
                    bool itemSold = false;
                    do
                    {
                        currentInventoryX = CurrentSellingSlotNumber % 6;
                        currentInventoryY = CurrentSellingSlotNumber / 6;
                        Item currentItem = playerChar.getItemFromInventoryAndSetNull(currentInventoryX, currentInventoryY);
                        if (currentItem != null)
                        {
                            GameMaster.getInstance().Log.AddToLog("The Hero sold " + currentItem.Name + " for " + currentItem.Price.ToString() + " gold");
                            playerChar.Gold += currentItem.Price;
                            Destroy(currentItem.gameObject);
                            itemSold = true;
                            RemainingTicksForCurrentItem = TicksPerSellingItem;
                        }
                        ++CurrentSellingSlotNumber;
                    } while (!itemSold && CurrentSellingSlotNumber < 18);

                    if (CurrentSellingSlotNumber >= 18)
                    {
                        CurrentState = TownState.ReadyForDungeon;
                    }
                }
                break;
        }
    }

}
