﻿using UnityEngine;
using System.Collections;

public class EpicLog : MonoBehaviour {

    public UnityEngine.UI.Text LogText;
    public UnityEngine.UI.Scrollbar LogScrollbar;

	// Use this for initialization
	void Start () {
	
	}

    public void AddToLog(string text)
    {
        bool scrollToBottom = LogScrollbar != null && LogScrollbar.value == 0;
        LogText.text += text;
        LogText.text += "\n";

        if (LogText.text.Length > 1000)
        {
            int numberOfCharsToDelete = LogText.text.Length - 1000;
            LogText.text = LogText.text.Substring(numberOfCharsToDelete);
        }

        if (scrollToBottom)
        {
            LogScrollbar.value = 0;
        }
    }
	
}
