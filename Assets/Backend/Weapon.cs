﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DamageEffect
{
    public DamageTypes DamageType;
    public int FromBaseDamage, ToBaseDamage;
    public BaseAttributes Attribute;
    public float FromAttributePercentage, ToAttributePercentage;
}

public class Weapon : MonoBehaviour {

    private string Name;

    public int TicksBetweenAttacks;
    private int TicksToNextAttack;

    public List<DamageEffect> DamageEffects = new List<DamageEffect>();

    public void setName(string name)
    {
        Name = name;
    }

    virtual public string getDescriptionString()
    {
        string descriptionString = "";

        string modFormatString = "#;-#;0";
        string modPercentFormatString = "#%;-#%;0%";

        descriptionString += TicksBetweenAttacks.ToString(modFormatString) + " ticks between attacks\n";

        foreach (DamageEffect effect in DamageEffects)
        {
            if (effect.ToBaseDamage != 0)
            {
                descriptionString += effect.FromBaseDamage.ToString(modFormatString) + " to " + effect.ToBaseDamage.ToString(modFormatString) + " " + BaseStats.getDamageTypeString(effect.DamageType) + " damage\n";
            }
            if (effect.ToAttributePercentage != 0)
            {
                descriptionString += effect.FromAttributePercentage.ToString(modPercentFormatString) + " to " + effect.ToAttributePercentage.ToString(modPercentFormatString) + " " + BaseStats.getAttributeTypeString(effect.Attribute) + " " + BaseStats.getDamageTypeString(effect.DamageType) + " damage\n";
            }

        }

        return descriptionString;
    }

    virtual public void multiplyWithFactor(float factor)
    {
        foreach (DamageEffect damEff in DamageEffects)
        {
            damEff.FromBaseDamage = (int)(damEff.FromBaseDamage * factor);
            damEff.ToBaseDamage = (int)(damEff.ToBaseDamage * factor);
            damEff.FromAttributePercentage *= factor;
            damEff.ToAttributePercentage *= factor;
        }
    }

    virtual public void tick(BaseStats baseStats)
    {
        TicksToNextAttack -= 1;
    }

    public bool isReadyToAttack()
    {
        return TicksToNextAttack <= 0;
    }

    virtual public List<DamageEffect> Attack(BaseStats ownStats, BaseStats enemyStats)
    {
        TicksToNextAttack = TicksBetweenAttacks;
        return DamageEffects;
    }

    virtual public string getAttackString(BaseStats ownStats, BaseStats enemyStats, float damage, DamageTypes damageType, bool critical)
    {
        return ownStats.Name + " hits " + enemyStats.Name + " with " + Name + " for " + damage.ToString("#;-#;0") + (critical ? " critical " : " ") + BaseStats.getDamageTypeString(damageType) + " damage.";
    }

    public virtual void gotAttackedByEnemy(BaseStats ownStats, BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed) { }
    public virtual void attackedEnemy(BaseStats ownStats, BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed) { }
    public virtual void killedEnemy(BaseStats ownStats, BaseStats enemyStats) { }
    public virtual void basicStatsChanged(BaseStats ownStats) { }
}
