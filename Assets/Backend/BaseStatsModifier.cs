﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseStatsModifierCollection
{
    public int MaxHealthMod, MaxManaMod,
        StrengthMod, AgilityMod, IntelligenceMod, ConstitutionMod;
    public float MaxHealthPercentMod, MaxManaPercentMod,
        StrengthPercentMod, AgilityPercentMod, IntelligencePercentMod, ConstitutionPercentMod;

    public int ArmorMod, PoisonResistanceMod, FireResistanceMod, IceResistanceMod, LightningResistanceMod;
    public float ArmorPercentMod, PoisonResistancePercentMod, FireResistancePercentMod,
        IceResistancePercentMod, LightningResistancePercentMod;

    public float HealthPerTick, ManaPerTick;

    public float DodgeChanceMod, BlockChanceMod, CriticalHitChanceMod;

    public void clear()
    {
        MaxHealthMod = MaxManaMod = StrengthMod = AgilityMod = IntelligenceMod = ConstitutionMod = 0;
        MaxHealthPercentMod = MaxManaPercentMod = StrengthPercentMod = AgilityPercentMod = IntelligencePercentMod = ConstitutionPercentMod = 0.0f;
        HealthPerTick = ManaPerTick = 0;
        ArmorMod = PoisonResistanceMod = FireResistanceMod = IceResistanceMod = LightningResistanceMod = 0;
        ArmorPercentMod = PoisonResistancePercentMod = FireResistancePercentMod = IceResistancePercentMod = LightningResistancePercentMod = 0.0f;
        DodgeChanceMod = BlockChanceMod = CriticalHitChanceMod = 0;
    }

    public void applyBaseModification(BaseStats baseStats)
    {
        baseStats.Strength += StrengthMod;
        baseStats.Agility += AgilityMod;
        baseStats.Intelligence += IntelligenceMod;
        baseStats.Constitution += ConstitutionMod;

        baseStats.Armor += ArmorMod;
        baseStats.PoisonResistance += PoisonResistanceMod;
        baseStats.FireResistance += FireResistanceMod;
        baseStats.IceResistance += IceResistanceMod;
        baseStats.LightningResistance += LightningResistanceMod;

        baseStats.Strength *= StrengthPercentMod;
        baseStats.Agility *= AgilityPercentMod;
        baseStats.Intelligence *= IntelligencePercentMod;
        baseStats.Constitution *= ConstitutionPercentMod;

        baseStats.Armor *= ArmorPercentMod;
        baseStats.PoisonResistance *= PoisonResistancePercentMod;
        baseStats.FireResistance *= FireResistancePercentMod;
        baseStats.IceResistance *= IceResistancePercentMod;
        baseStats.LightningResistance *= LightningResistancePercentMod;
    }

    public void applyDependentModifications(BaseStats baseStats)
    {
        baseStats.MaxHealth += MaxHealthMod;
        baseStats.MaxMana += MaxManaMod;

        baseStats.MaxHealth *= MaxHealthPercentMod;
        baseStats.MaxMana *= MaxManaPercentMod;

        baseStats.DodgeChance += DodgeChanceMod;
        baseStats.BlockChance += BlockChanceMod;
        baseStats.CriticalHitChance += CriticalHitChanceMod;
    }
}



public class BaseStatsModifier : MonoBehaviour {

    public int MaxHealthMod, MaxManaMod,
        StrengthMod, AgilityMod, IntelligenceMod, ConstitutionMod;
    public float MaxHealthPercentMod, MaxManaPercentMod, 
        StrengthPercentMod, AgilityPercentMod, IntelligencePercentMod, ConstitutionPercentMod;

    public int ArmorMod, PoisonResistanceMod, FireResistanceMod, IceResistanceMod, LightningResistanceMod;
    public float ArmorPercentMod, PoisonResistancePercentMod, FireResistancePercentMod,
        IceResistancePercentMod, LightningResistancePercentMod;

    public float HealthPerTick, ManaPerTick;

    public float DodgeChanceMod, BlockChanceMod, CriticalHitChanceMod;


    public BaseStatsModifier()
    {
        clear();
	}

    public virtual string getDescriptionString()
    {
        string modifierString = "";

        string modFormatString = "+#;-#;0";
        string modPercentFormatString = "+#%;-#%;0%";
        if (MaxHealthMod != 0)
        {
            modifierString += "Max Health " + MaxHealthMod.ToString(modFormatString) + "\n";
        }
        if (MaxManaMod != 0)
        {
            modifierString += "Max Mana " + MaxManaMod.ToString(modFormatString) + "\n";
        }
        if (StrengthMod != 0)
        {
            modifierString += "Strength " + StrengthMod.ToString(modFormatString) + "\n";
        }
        if (AgilityMod != 0)
        {
            modifierString += "Agility " + AgilityMod.ToString(modFormatString) + "\n";
        }
        if (IntelligenceMod != 0)
        {
            modifierString += "Intelligence " + IntelligenceMod.ToString(modFormatString) + "\n";
        }
        if (ConstitutionMod != 0)
        {
            modifierString += "Constitution " + ConstitutionMod.ToString(modFormatString) + "\n";
        }

        if (MaxHealthPercentMod != 0)
        {
            modifierString += "Max Health " + MaxHealthPercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (MaxManaPercentMod != 0)
        {
            modifierString += "Max Mana " + MaxManaPercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (StrengthPercentMod != 0)
        {
            modifierString += "Strength " + StrengthPercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (AgilityPercentMod != 0)
        {
            modifierString += "Agility " + AgilityPercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (IntelligencePercentMod != 0)
        {
            modifierString += "Intelligence " + IntelligencePercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (ConstitutionPercentMod != 0)
        {
            modifierString += "Constitution " + ConstitutionPercentMod.ToString(modPercentFormatString) + "\n";
        }

        if (ArmorMod != 0)
        {
            modifierString += "Armor " + ArmorMod.ToString(modFormatString) + "\n";
        }
        if (PoisonResistanceMod != 0)
        {
            modifierString += "Poison resist " + PoisonResistanceMod.ToString(modFormatString) + "\n";
        }
        if (FireResistanceMod != 0)
        {
            modifierString += "Fire resist " + FireResistanceMod.ToString(modFormatString) + "\n";
        }
        if (IceResistanceMod != 0)
        {
            modifierString += "Ice resist " + IceResistanceMod.ToString(modFormatString) + "\n";
        }
        if (LightningResistanceMod != 0)
        {
            modifierString += "Lightning resist " + LightningResistanceMod.ToString(modFormatString) + "\n";
        }

        if (ArmorPercentMod != 0)
        {
            modifierString += "Armor " + ArmorPercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (PoisonResistancePercentMod != 0)
        {
            modifierString += "Poison resist" + PoisonResistancePercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (FireResistancePercentMod != 0)
        {
            modifierString += "Fire resist " + FireResistancePercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (IceResistancePercentMod != 0)
        {
            modifierString += "Ice resist " + IceResistancePercentMod.ToString(modPercentFormatString) + "\n";
        }
        if (LightningResistancePercentMod != 0)
        {
            modifierString += "Lightning resist " + LightningResistancePercentMod.ToString(modPercentFormatString) + "\n";
        }

        if (HealthPerTick != 0)
        {
            modifierString += "Health per tick " + HealthPerTick.ToString(modFormatString) + "\n";
        }
        if (ManaPerTick != 0)
        {
            modifierString += "Mana per tick " + ManaPerTick.ToString(modFormatString) + "\n";
        }

        if (DodgeChanceMod != 0)
        {
            modifierString += "Dodge chance " + DodgeChanceMod.ToString(modPercentFormatString) + "\n";
        }
        if (BlockChanceMod != 0)
        {
            modifierString += "Block chance " + BlockChanceMod.ToString(modPercentFormatString) + "\n";
        }
        if (CriticalHitChanceMod != 0)
        {
            modifierString += "Critical hit chance " + CriticalHitChanceMod.ToString(modPercentFormatString) + "\n";
        }

        return modifierString;
    }

    virtual public void multiplyWithFactor(float factor)
    {
        MaxHealthMod = (int)(MaxHealthMod * factor);
        MaxManaMod = (int)(MaxManaMod * factor);
        StrengthMod = (int)(StrengthMod * factor);
        AgilityMod = (int)(AgilityMod * factor);
        IntelligenceMod = (int)(IntelligenceMod * factor);
        ConstitutionMod = (int)(ConstitutionMod * factor);
        //MaxHealthPercentMod *= factor;
        //MaxManaPercentMod *= factor; 
        //StrengthPercentMod *= factor; 
        //AgilityPercentMod *= factor;
        //IntelligencePercentMod *= factor;
        //ConstitutionPercentMod *= factor;

        ArmorMod = (int)(ArmorMod * factor);
        PoisonResistanceMod = (int)(PoisonResistanceMod * factor);
        FireResistanceMod = (int)(FireResistanceMod * factor);
        IceResistanceMod = (int)(IceResistanceMod * factor);
        LightningResistanceMod = (int)(LightningResistanceMod * factor);
        //ArmorPercentMod *= factor;
        //PoisonResistancePercentMod *= factor;
        //FireResistancePercentMod *= factor;
        //IceResistancePercentMod *= factor;
        //LightningResistancePercentMod *= factor;

        HealthPerTick *= factor;
        ManaPerTick *= factor;

        //DodgeChanceMod *= factor;
        //BlockChanceMod *= factor;
    }

    public void clear()
    {
        MaxHealthMod = MaxManaMod = StrengthMod = AgilityMod = IntelligenceMod = ConstitutionMod = 0;
        MaxHealthPercentMod = MaxManaPercentMod = StrengthPercentMod = AgilityPercentMod = IntelligencePercentMod = ConstitutionPercentMod = 0.0f;
        HealthPerTick = ManaPerTick = 0;
        ArmorMod = PoisonResistanceMod = FireResistanceMod = IceResistanceMod = LightningResistanceMod = 0;
        ArmorPercentMod = PoisonResistancePercentMod = FireResistancePercentMod = IceResistancePercentMod = LightningResistancePercentMod = 0.0f;
        DodgeChanceMod = BlockChanceMod = CriticalHitChanceMod = 0.0f;
    }

    public void addToModifier(BaseStatsModifierCollection modifier) {
        modifier.StrengthMod += StrengthMod;
        modifier.AgilityMod += AgilityMod;
        modifier.IntelligenceMod += IntelligenceMod;
        modifier.ConstitutionMod += ConstitutionMod;
        
        modifier.ArmorMod += ArmorMod;
        modifier.PoisonResistanceMod += PoisonResistanceMod;
        modifier.FireResistanceMod += FireResistanceMod;
        modifier.IceResistanceMod += IceResistanceMod;
        modifier.LightningResistanceMod += LightningResistanceMod;

        modifier.StrengthPercentMod += StrengthPercentMod;
        modifier.AgilityPercentMod += AgilityPercentMod;
        modifier.IntelligencePercentMod += IntelligencePercentMod;
        modifier.ConstitutionPercentMod += ConstitutionPercentMod;

        modifier.ArmorPercentMod += ArmorPercentMod;
        modifier.PoisonResistancePercentMod += PoisonResistancePercentMod;
        modifier.FireResistancePercentMod += FireResistancePercentMod;
        modifier.IceResistancePercentMod += IceResistancePercentMod;
        modifier.LightningResistancePercentMod += LightningResistancePercentMod;

        modifier.MaxHealthMod += MaxHealthMod;
        modifier.MaxManaMod += MaxManaMod;

        modifier.MaxHealthPercentMod += MaxHealthPercentMod;
        modifier.MaxManaPercentMod += MaxManaPercentMod;

        modifier.HealthPerTick += HealthPerTick;
        modifier.ManaPerTick += ManaPerTick;

        modifier.DodgeChanceMod += DodgeChanceMod;
        modifier.BlockChanceMod += BlockChanceMod;
        modifier.CriticalHitChanceMod += CriticalHitChanceMod;
    }

    public virtual void applySpecialEffects(BaseStats baseStats) { }
    public virtual void tick(BaseStats baseStats) { }
    public virtual void gotAttackedByEnemy(BaseStats ownStats, BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed) { }
    public virtual void attackedEnemy(BaseStats ownStats, BaseStats enemyStats, float damage, DamageTypes damageType, bool critical, bool blocked, bool missed) { }
    public virtual void killedEnemy(BaseStats ownStats, BaseStats enemyStats) { }
    public virtual void basicStatsChanged(BaseStats ownStats) { }
}
