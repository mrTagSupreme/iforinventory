﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DungeonWithLevel
{
    public GameObject DungeonPrefab;
    public int MinLevel, MaxLevel;
}

public class DungeonDatabase : MonoBehaviour {

    public List<DungeonWithLevel> Dungeons = new List<DungeonWithLevel>();
    public UnityEngine.UI.Text MonsterTitleText, MonsterStatsText;

    public Dungeon getDungeon(int heroLevel)
    {
        List<DungeonWithLevel> dungeonsForLevel = new List<DungeonWithLevel>();
        foreach (DungeonWithLevel dungeon in Dungeons)
        {
            if (dungeon.MinLevel <= heroLevel && dungeon.MaxLevel >= heroLevel)
                dungeonsForLevel.Add(dungeon);
        }

        DungeonWithLevel chosenDungeon = dungeonsForLevel[Random.Range(0, dungeonsForLevel.Count)];
        GameObject dungeonObject = (GameObject)GameObject.Instantiate(chosenDungeon.DungeonPrefab);
        Dungeon dungeonComponent = dungeonObject.GetComponent<Dungeon>();
        dungeonComponent.init(heroLevel, MonsterTitleText, MonsterStatsText);
        return dungeonComponent;
    }
}
