﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GameState
{
    StartScreen,
    HeroInTown,
    HeroInDungeon
}

public class GameMaster : MonoBehaviour {

    private static GameMaster Instance;
    public static GameMaster getInstance()
    {
        return Instance;
    }

    public float TimePerTick = 0.25f;
    private float TimeToNextTick;

    private GameState State;

    private Character Hero;
    private Dungeon CurrentDungeon;
    public Town TownObj;

    public ItemDatabase Items;
    public DungeonDatabase Dungeons;
    public EpicLog Log;
    public PlayerGUIUpdater PlayerGUI;
    public SlotGUI PlayerSlotGUI;

	// Use this for initialization
	void Start () {
        Random.seed = (int)System.DateTime.Now.Ticks;
        Instance = this;
        Log.AddToLog("The Hero hears a call for adventure...");
        State = GameState.HeroInDungeon;
        CurrentDungeon = Dungeons.getDungeon(1);
        CurrentDungeon.transform.parent = transform;
        Log.AddToLog(CurrentDungeon.EnterDescription);

        GameObject playerObject = createRandomPlayer();
        playerObject.transform.parent = transform;
        Hero = playerObject.GetComponent<Character>();
        PlayerGUI.setPlayer(Hero);
        PlayerSlotGUI.init(Hero);
	}
	
	void FixedUpdate () {
        TimeToNextTick -= Time.fixedDeltaTime;
        if (TimeToNextTick <= 0)
        {
            switch (State)
            {
                case GameState.HeroInDungeon:
                    Hero.tick();
                    CurrentDungeon.tick(Hero);

                    switch (CurrentDungeon.CurrentState)
                    {
                        case Dungeon.State.FightingMonster:
                            calculateAttacks(Hero.Stats, CurrentDungeon.CurrentMonster.Stats);
                            if (CurrentDungeon.CurrentMonster.Stats.Health <= 0)
                            {
                                // hero killed tha monsta!
                                Log.AddToLog(CurrentDungeon.CurrentMonster.Stats.Name + " was slain by " + Hero.Stats.Name);
                                Hero.Stats.killedEnemy(CurrentDungeon.CurrentMonster.Stats);
                                Hero.Stats.addXP(CurrentDungeon.CurrentMonster.AddXP);
                            }
                            else
                            {
                                // monster attacks
                                calculateAttacks(CurrentDungeon.CurrentMonster.Stats, Hero.Stats);
                            }

                            if (Hero.Stats.Health <= 0)
                            {
                                // hero is dead!
                                Log.AddToLog(Hero.Stats.Name + " was slain by " + CurrentDungeon.CurrentMonster.Stats.Name);
                                Destroy(Hero);
                                Hero = null;
                                Destroy(CurrentDungeon);
                                CurrentDungeon = null;
                                State = GameState.StartScreen;
                            }
                            break;
                       case Dungeon.State.Finished:
                            Log.AddToLog(CurrentDungeon.FinishedDescription);
                            Destroy(CurrentDungeon);
                            CurrentDungeon = null;
                            State = GameState.HeroInTown;
                            TownObj.enterTown(Hero);
                            break;
                    }
                    
                    break;
                case GameState.HeroInTown:
                    Hero.Stats.tick();
                    TownObj.tick(Hero);
                    if (TownObj.CurrentState == TownState.ReadyForDungeon)
                    {
                        CurrentDungeon = Dungeons.getDungeon(Hero.Stats.Level);
                        CurrentDungeon.transform.parent = transform;
                        State = GameState.HeroInDungeon;
                        Log.AddToLog("\n" + CurrentDungeon.EnterDescription);
                    }
                    break;
                case GameState.StartScreen:

                    break;
            }
            TimeToNextTick = TimePerTick;
        }
	}

    static void calculateAttacks(BaseStats attacker, BaseStats enemy)
    {
        foreach (Weapon weap in attacker.Weapons)
        {
            if (weap.isReadyToAttack())
            {
                List<DamageEffect> damageEffects = weap.Attack(attacker, enemy);
                foreach (DamageEffect damage in damageEffects)
                {
                    float calculatedDamage = 0;
                    if (damage.ToBaseDamage != 0)
                    {
                        calculatedDamage += Random.Range(damage.FromBaseDamage, damage.ToBaseDamage + 1);
                    }
                    if (damage.ToAttributePercentage != 0)
                    {
                        float attributeValue = attacker.getAttribute(damage.Attribute);
                        calculatedDamage += Random.Range(damage.FromAttributePercentage * attributeValue, damage.ToAttributePercentage * attributeValue);
                    }
                    calculatedDamage -= enemy.getResistance(damage.DamageType);
                    calculatedDamage = Mathf.Max(0, calculatedDamage);
                    bool critical = Random.value < attacker.CriticalHitChance;
                    if(critical)
                    {
                        // critical!
                        calculatedDamage *= 2.0f;
                    }

                    // enemy can still dodge or block the attack!
                    bool missed = Random.value < enemy.DodgeChance;
                    if (missed)
                    {
                        GameMaster.getInstance().Log.AddToLog(attacker.Name + " missed!");
                    }
                    bool blocked = !missed && Random.value < enemy.BlockChance;
                    if (blocked)
                    {
                        GameMaster.getInstance().Log.AddToLog(enemy.Name + " blocked the attack!");
                    }

                    attacker.attackedEnemy(enemy, calculatedDamage, damage.DamageType, critical, blocked, missed);
                    GameMaster.getInstance().Log.AddToLog(weap.getAttackString(attacker, enemy, calculatedDamage, damage.DamageType, critical));

                    if (!blocked && !missed)
                    {
                        enemy.addHealth(-calculatedDamage);
                    }

                    if (enemy.Health <= 0) break;
                    enemy.gotAttackedByEnemy(attacker, calculatedDamage, damage.DamageType, critical, blocked, missed);
                }
                if (enemy.Health <= 0) break;
            }
        }
    }

    public GameObject createRandomPlayer()
    {
        GameObject player = new GameObject();
        BaseStats playerStats = player.AddComponent<BaseStats>();

        playerStats.Name = "The Hero";
        playerStats.addRandomAttributePoints(25);
        playerStats.addXP(0); // sorgt dafür, das er auf lvl 1 aufsteigt...

        // wird beim ersten update geclampt!
        playerStats.Health = 99999;
        //playerStats.Mana = 9999;

        Character playerCharacter = player.AddComponent<Character>();
        playerCharacter.Stats = playerStats;
        GameObject startingItem = GameObject.Instantiate(Items.StartItems[0]) as GameObject;
        Item item = startingItem.GetComponent<Item>();
        item.init(1);
        playerCharacter.setEquippedItem(EquipmentSlots.HandLefSlot, item);
        return player;
    }
}
