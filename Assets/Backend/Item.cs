﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class OptionalModifier
{
    public BaseStatsModifier Modifier;
    public int StartingLevel;
    public float Chance;
}

public class Item : MonoBehaviour {

    public string Name;

    [HideInInspector]
    public string Description;

    [EnumFlag]
    public EquipmentSlots AllowedSlots;
    public int MinLevel;
    public Texture ItemTexture;
    public int Price;
    public bool ModifyPriceWithLevel = true;

    Consumable ConsumableComponent;

    public List<OptionalModifier> OptionalModifiers = new List<OptionalModifier>();

	// Use this for initialization
	void Start () {
        
	}

	public bool canGoIntoSlot(EquipmentSlots slot)
	{
		return (AllowedSlots & slot) == slot;
	}

    public bool canBeConsumed()
    {
        return ConsumableComponent != null;
    }

    public void consume(BaseStats stats)
    {
        ConsumableComponent.consume(stats);
    }

    public void init(int level)
    {
        ConsumableComponent = GetComponent<Consumable>();

        Description = "";
        MinLevel = level;

        foreach (OptionalModifier optMod in OptionalModifiers)
        {
            if (level < optMod.StartingLevel || Random.value < optMod.Chance)
            {
                Destroy(optMod.Modifier);
            }
        }
        OptionalModifiers.Clear();

        float levelFactor = BaseStats.getLevelFactor(level);
        Weapon[] weapons = GetComponents<Weapon>();
        foreach (Weapon weap in weapons)
        {
            weap.multiplyWithFactor(levelFactor * Random.Range(0.95f, 1.05f));
            weap.setName(Name);
            Description += weap.getDescriptionString();
        }

        BaseStatsModifier[] modifier = GetComponents<BaseStatsModifier>();
        foreach (BaseStatsModifier mod in modifier)
        {
            mod.multiplyWithFactor(levelFactor * Random.Range(0.95f, 1.05f));
            Description += mod.getDescriptionString();
        }

        if (ConsumableComponent)
        {
            ConsumableComponent.modifyForLevel(level);
            Description += ConsumableComponent.getDescription();
        }

        if (ModifyPriceWithLevel)
        {
            Price = (int)(Price * levelFactor);
        }

        Description += "Price: " + Price.ToString();
    }
}
