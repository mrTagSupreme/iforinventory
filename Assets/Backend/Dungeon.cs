﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dungeon : MonoBehaviour
{
    private UnityEngine.UI.Text MonsterTitleText, MonsterStatsText;

    public enum State
    {
        FightingMonster,
        WanderingAround,
        Looting,
        Finished
    } 
    public State CurrentState { get; private set; }

    private int BaseLevel;

    public List<GameObject> Loot;
    public List<GameObject> EpicLoot;
    public List<GameObject> Minions;
    public GameObject BossMonster;
    public string EnterDescription, FinishedDescription;

    public Monster CurrentMonster { get; private set; }

    private const int TicksForOneLoot = 6;
    private int RemainingTicksForCurrentLoot = 0;
    private int RemainingLootCount;
    private int RemainingEpicLootCount;

    public int NumberOfMinions;
    private int RemainingMinions;
    private bool BossFightDone;
    private int RemainingTicksToNextFight;

    public void init(int heroLevel, UnityEngine.UI.Text monsterTitleText, UnityEngine.UI.Text monsterStatsText)
    {
        BaseLevel = heroLevel;
        RemainingMinions = NumberOfMinions;
        BossFightDone = false;
        RemainingTicksToNextFight = Random.Range(8, 20);
        CurrentState = State.WanderingAround;
        RemainingTicksToNextFight = Random.Range(8, 20);
        MonsterTitleText = monsterTitleText;
        MonsterStatsText = monsterStatsText;
    }

    public void tick(Character heroChar){
        switch(CurrentState){
            case State.WanderingAround:
                RemainingTicksToNextFight -= 1;
                if (RemainingTicksToNextFight <= 0) {
                    if (RemainingMinions <= 0)
                    {
                        CurrentMonster = getBossMonster();
                    }
                    else
                    {
                        CurrentMonster = getRandomMinion();
                    }
                    GameMaster.getInstance().Log.AddToLog("\nThe Hero encounters " + CurrentMonster.Stats.Name + " (Lvl " + CurrentMonster.Stats.Level.ToString() + ")");
                    CurrentMonster.transform.parent = transform;
                    CurrentMonster.Stats.tick();
                    CurrentState = State.FightingMonster;

                    MonsterTitleText.text = CurrentMonster.Stats.Name + " \nLvl " + CurrentMonster.Stats.Level.ToString();
                    MonsterStatsText.text = "Health: " + CurrentMonster.Stats.Health;
                }
                break;
            case State.FightingMonster:
                if (CurrentMonster.Stats.Health <= 0)
                {
                    CurrentState = State.Looting;
                    RemainingLootCount = Random.Range(0, CurrentMonster.LootCount+1);
                    RemainingEpicLootCount = CurrentMonster.EpicLootCount;
                    RemainingTicksForCurrentLoot = TicksForOneLoot;
                    if (CurrentMonster.Char.Gold > 0)
                    {
                        GameMaster.getInstance().Log.AddToLog(CurrentMonster.Stats.Name + " dropped " + CurrentMonster.Char.Gold.ToString() + " gold");
                    }
                    Destroy(CurrentMonster);
                    CurrentMonster = null;
                    if (RemainingMinions <= 0)
                    {
                        BossFightDone = true;
                    }
                    else
                    {
                        RemainingMinions -= 1;
                    }
                    MonsterStatsText.text = "Dead";
                }
                else
                {
                    CurrentMonster.Stats.tick();
                    MonsterStatsText.text = "Health: " + CurrentMonster.Stats.Health.ToString("#;0");
                }
                break;
            case State.Looting:
                RemainingTicksForCurrentLoot -= 1;
                if (RemainingTicksForCurrentLoot <= 0)
                {
                    if (RemainingEpicLootCount > 0)
                    {
                        Item epicLootItem = getRandomEpicLoot();
                        GameMaster.getInstance().Log.AddToLog("The Hero found " + epicLootItem.Name);
                        heroChar.putItemIntoRandomInventorySlot(epicLootItem);
                        RemainingEpicLootCount -= 1;
                    }
                    else if (RemainingLootCount > 0)
                    {
                        Item lootItem = getRandomLoot();
                        GameMaster.getInstance().Log.AddToLog("The Hero found " + lootItem.Name);
                        heroChar.putItemIntoRandomInventorySlot(lootItem);
                        RemainingLootCount -= 1;
                    }
                    RemainingTicksForCurrentLoot = TicksForOneLoot;

                    if (RemainingLootCount + RemainingEpicLootCount <= 0)
                    {
                        if (RemainingMinions <= 0 && BossFightDone)
                        {
                            CurrentState = State.Finished;
                        }
                        else
                        {
                            CurrentState = State.WanderingAround;
                            RemainingTicksToNextFight = Random.Range(8, 20);
                        }
                    }
                }
                break;
            case State.Finished:

                break;
        }
    }

    private Item getRandomLoot()
    {
        GameObject itemObject = (GameObject)GameObject.Instantiate(Loot[Random.Range(0, Loot.Count)]);
        Item item = itemObject.GetComponent<Item>();
        item.init(BaseLevel + Random.Range(-2, 3));

        return item;
    }

    private Item getRandomEpicLoot()
    {
        GameObject itemObject = (GameObject)GameObject.Instantiate(EpicLoot[Random.Range(0, EpicLoot.Count)]);
        Item item = itemObject.GetComponent<Item>();
        item.init(BaseLevel + Random.Range(-2, 3));

        return item;
    }

    private Monster getRandomMinion()
    {
        GameObject monsterObject = (GameObject)GameObject.Instantiate(Minions[Random.Range(0, Minions.Count)]);
        Monster monster = monsterObject.GetComponent<Monster>();
        monster.init(Mathf.Max(1, BaseLevel + Random.Range(-1, 2)));

        return monster;
    }

    private Monster getBossMonster()
    {
        GameObject monsterObject = (GameObject)GameObject.Instantiate(BossMonster);
        Monster monster = monsterObject.GetComponent<Monster>();
        monster.init(BaseLevel);

        return monster;
    }
}
