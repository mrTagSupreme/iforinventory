﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {

    public float AddXP;
    public int LootCount;
    public int EpicLootCount;

    public BaseStats Stats;
    public Character Char;

    virtual public void init(int level)
    {
        //float levelFactor = BaseStats.getLevelFactor(level);
        Stats = GetComponent<BaseStats>();

        Char = GetComponent<Character>();

        //AddXP *= levelFactor;

        Stats.addXP(BaseStats.getXPForLevel(level));

        Item[] items = GetComponentsInChildren<Item>();
        foreach (Item item in items)
        {
            item.init(level);
            Char.putItemIntoNextFreeSlot(item);
        }

        Stats.Health = 999999; // will be clamped in updatestats()

        Stats.MarkStatsAsDirty();
    }
	
}
