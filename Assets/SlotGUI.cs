﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlotGUI : MonoBehaviour {

	public GameObject mouseItem;
	public GameObject tooltip;

    public UnityEngine.UI.RawImage HeadSlot, NecklaceSlot, HandLeftSlot, HandRightSlot, 
        BodyArmorSlot, PantsSlot, FootLeftSlot, FootRightSlot, ArmbandSlot;

    public UnityEngine.UI.RawImage[] BackPackSlots = new UnityEngine.UI.RawImage[18];
    public UnityEngine.UI.RawImage[] QuickSlots = new UnityEngine.UI.RawImage[4];

    public Texture BGImageCommon, BGImageLegendary, BGImageUnidentified, BGImageUnique;

    private Dictionary<EquipmentSlots, UnityEngine.UI.RawImage> ImageSlotDictionary = new Dictionary<EquipmentSlots,UnityEngine.UI.RawImage>();
	private Character myChar;
	private bool isDraggingItem = false;

	// Use this for initialization
	void Start () {
		mouseItem.SetActive(false);
		tooltip.SetActive(false);
        ImageSlotDictionary[EquipmentSlots.BodyArmorSlot] = BodyArmorSlot;
        ImageSlotDictionary[EquipmentSlots.HandLefSlot] = HandLeftSlot;
        ImageSlotDictionary[EquipmentSlots.HandRightSlot] = HandRightSlot;
        ImageSlotDictionary[EquipmentSlots.HeadSlot] = HeadSlot;
        ImageSlotDictionary[EquipmentSlots.NecklaceSlot] = NecklaceSlot;
        ImageSlotDictionary[EquipmentSlots.PantsSlot] = PantsSlot;
        ImageSlotDictionary[EquipmentSlots.ShoeLeftSlot] = FootLeftSlot;
        ImageSlotDictionary[EquipmentSlots.ShoeRightSlot] = FootRightSlot;
        ImageSlotDictionary[EquipmentSlots.TrinketSlot] = ArmbandSlot;

        if (myChar)
        {
            updateSlots(myChar);
        }
	}



	public void Update() {
		if (isDraggingItem == true) {
			mouseItem.transform.position = new Vector2 (Input.mousePosition.x - 16, Input.mousePosition.y + 16);
		}
    }

	public void onDrop_Quickslot( int dropTarget) {
		Debug.Log("Dropped Quickslot " + dropTarget );

		int fromSlotX = mouseItem.GetComponent<MouseItem>().itemIsFromSlotX;
		int fromSlotY = mouseItem.GetComponent<MouseItem>().itemIsFromSlotY;
		Item itemInSlot = myChar.getQuickSlotItem (dropTarget);

		bool dropPossible = mouseItem.GetComponent<MouseItem>().draggedItem.canGoIntoSlot(EquipmentSlots.QuickSlot);
		
		if (dropPossible) {			
			if (itemInSlot != null) {
				myChar.setInventoryItem(fromSlotX, fromSlotY, itemInSlot);
				myChar.setQuickSlotItem(dropTarget, mouseItem.GetComponent<MouseItem>().draggedItem);
			} else {
				myChar.setQuickSlotItem(dropTarget, mouseItem.GetComponent<MouseItem>().draggedItem);
			}

		} else {
			Debug.Log ("Drop invalid");
			myChar.setInventoryItem(fromSlotX, fromSlotY, mouseItem.GetComponent<MouseItem>().draggedItem);
		}

		isDraggingItem = false;
		mouseItem.SetActive(false);
		mouseItem.GetComponent<MouseItem>().setItem(null);

	}

	public void onDrop_Inventory( int dropTarget) {
		Debug.Log("Dropped Inventory " + dropTarget );

		int fromSlotX = mouseItem.GetComponent<MouseItem>().itemIsFromSlotX;
		int fromSlotY = mouseItem.GetComponent<MouseItem>().itemIsFromSlotY;
		Item itemInSlot = myChar.getInventoryItem(dropTarget % 6, dropTarget / 6);


		if (itemInSlot != null) {
			myChar.setInventoryItem(fromSlotX, fromSlotY, itemInSlot);
			myChar.setInventoryItem(dropTarget % 6, dropTarget / 6, mouseItem.GetComponent<MouseItem>().draggedItem);
		} else {
			myChar.setInventoryItem(dropTarget % 6, dropTarget / 6, mouseItem.GetComponent<MouseItem>().draggedItem);
		}
		
		isDraggingItem = false;
		mouseItem.SetActive(false);
		mouseItem.GetComponent<MouseItem>().setItem(null);

	}

	public void showTooltip_Inventory(int slotID) {
		Item tooltipItem = myChar.getInventoryItem(slotID % 6, slotID / 6);
		//Debug.Log ("tooltipItem " + tooltipItem);
		if (tooltipItem != null) {
			tooltip.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Name;
			tooltip.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Description;
			tooltip.SetActive(true);
		}
		
	}
	
	public void showTooltip_Quickslot(int slotID) {
		Item tooltipItem = myChar.getQuickSlotItem(slotID);
		//Debug.Log ("tooltipItem " + tooltipItem);
		if (tooltipItem != null) {
			tooltip.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Name;
            tooltip.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Description;
			tooltip.SetActive(true);
		}
		
	}
	
	
	public void showTooltip_Equipment(int slotID) {
		Item tooltipItem = null;
		EquipmentSlots tooltipEnum = EquipmentSlots.None;
		
		switch (slotID) {
		case 0: tooltipEnum = EquipmentSlots.BodyArmorSlot; break;
		case 1: tooltipEnum = EquipmentSlots.HandLefSlot; break;
		case 2:	tooltipEnum = EquipmentSlots.HandRightSlot; break;
		case 3:	tooltipEnum = EquipmentSlots.HeadSlot;	break;
		case 4:	tooltipEnum = EquipmentSlots.NecklaceSlot;	break;
		case 5:	tooltipEnum = EquipmentSlots.PantsSlot; break;
		case 6:	tooltipEnum = EquipmentSlots.ShoeLeftSlot;	break;
		case 7:	tooltipEnum = EquipmentSlots.ShoeRightSlot;	break;
		case 8:	tooltipEnum = EquipmentSlots.TrinketSlot;	break;
		}
		
		tooltipItem = myChar.getEquippedItem(tooltipEnum);
		Debug.Log ("tooltipItem " + tooltipItem);
		if (tooltipItem != null) {
			tooltip.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Name;
            tooltip.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = tooltipItem.Description;
			tooltip.SetActive(true);
		}
		
	}
	
	
	public void tooltipHide() {
		tooltip.SetActive(false);
	}

	public void onDrop_Elsewhere( ) {
		Debug.Log ("dropped elsewhere");

		int fromSlotX = mouseItem.GetComponent<MouseItem>().itemIsFromSlotX;
		int fromSlotY = mouseItem.GetComponent<MouseItem>().itemIsFromSlotY;
		myChar.setInventoryItem(fromSlotX, fromSlotY, mouseItem.GetComponent<MouseItem>().draggedItem);

		isDraggingItem = false;
		mouseItem.SetActive(false);
		mouseItem.GetComponent<MouseItem>().setItem(null);
	}

	public void onDrop_Equipment( int dropTarget) {
		Debug.Log("Dropped Equipment " + dropTarget );

		Item droppedItem = null;
		EquipmentSlots droppedEnum = EquipmentSlots.None;
		
		switch (dropTarget) {
			case 0: droppedEnum = EquipmentSlots.BodyArmorSlot; break;
			case 1: droppedEnum = EquipmentSlots.HandLefSlot; break;
			case 2:	droppedEnum = EquipmentSlots.HandRightSlot; break;
			case 3:	droppedEnum = EquipmentSlots.HeadSlot;	break;
			case 4:	droppedEnum = EquipmentSlots.NecklaceSlot;	break;
			case 5:	droppedEnum = EquipmentSlots.PantsSlot; break;
			case 6:	droppedEnum = EquipmentSlots.ShoeLeftSlot;	break;
			case 7:	droppedEnum = EquipmentSlots.ShoeRightSlot;	break;
			case 8:	droppedEnum = EquipmentSlots.TrinketSlot;	break;
		}
		droppedItem = myChar.getEquippedItem(droppedEnum);

		int fromSlotX = mouseItem.GetComponent<MouseItem>().itemIsFromSlotX;
		int fromSlotY = mouseItem.GetComponent<MouseItem>().itemIsFromSlotY;

		if (droppedItem != null) {
			Debug.Log ("Slot already taken by " + droppedItem.name);

			myChar.putObjectFromEquipementToInventory(droppedEnum,  fromSlotX, fromSlotY);
			myChar.setEquippedItem(droppedEnum, mouseItem.GetComponent<MouseItem>().draggedItem);

			mouseItem.SetActive(false);
			mouseItem.GetComponent<MouseItem>().setItem(null);

		} else {
			Debug.Log ("Dropped the thing in a free slot");

			bool dropPossible = mouseItem.GetComponent<MouseItem>().draggedItem.canGoIntoSlot(droppedEnum);

			if (dropPossible) {
				Debug.Log ("Drop possible");
				myChar.setEquippedItem(droppedEnum, mouseItem.GetComponent<MouseItem>().draggedItem);
			} else {
				Debug.Log ("Drop invalid");
				myChar.setInventoryItem(fromSlotX, fromSlotY, mouseItem.GetComponent<MouseItem>().draggedItem);
			}

		}

		isDraggingItem = false;
		mouseItem.SetActive(false);
        mouseItem.GetComponent<MouseItem>().setItem(null);
	}

	public void startDrag_Inventory( int slotDragged ) {
		Item draggedItem = myChar.getItemFromInventoryAndSetNull(slotDragged % 6, slotDragged / 6);
		if (draggedItem != null) {
			Debug.Log ("Pulled the thing " + draggedItem.name);
			isDraggingItem = true;			
			mouseItem.SetActive(true);
			mouseItem.GetComponent<MouseItem>().setItem(draggedItem);
			mouseItem.GetComponent<MouseItem>().itemIsFromSlotX = slotDragged % 6;
			mouseItem.GetComponent<MouseItem>().itemIsFromSlotY = slotDragged / 6;
		}
	}

	public void startDrag_Quickslot( int slotDragged ) {
		Item draggedItem = myChar.getItemFromQuickSlotAndSetNull (slotDragged);
		if (draggedItem != null) {
			Debug.Log ("Pulled the thing " + draggedItem.name);
			isDraggingItem = true;
			mouseItem.SetActive(true);
			mouseItem.GetComponent<MouseItem>().setItem(draggedItem);
			mouseItem.GetComponent<MouseItem>().itemIsFromQuickSlot = slotDragged;
		}
	}

	public void startDrag_Equipment( int slotDragged ) {

		Item draggedItem = null;
		EquipmentSlots draggedEnum = EquipmentSlots.None;
		
		switch (slotDragged) {
		case 0: draggedEnum = EquipmentSlots.BodyArmorSlot; break;
		case 1: draggedEnum = EquipmentSlots.HandLefSlot; break;
		case 2:	draggedEnum = EquipmentSlots.HandRightSlot; break;
		case 3:	draggedEnum = EquipmentSlots.HeadSlot;	break;
		case 4:	draggedEnum = EquipmentSlots.NecklaceSlot;	break;
		case 5:	draggedEnum = EquipmentSlots.PantsSlot; break;
		case 6:	draggedEnum = EquipmentSlots.ShoeLeftSlot;	break;
		case 7:	draggedEnum = EquipmentSlots.ShoeRightSlot;	break;
		case 8:	draggedEnum = EquipmentSlots.TrinketSlot;	break;
		}
		draggedItem = myChar.getItemFromEquipmentAndSetNull(draggedEnum);

		if (draggedItem != null) {
			Debug.Log ("Pulled the thing " + draggedItem.name);

			isDraggingItem = true;			
			mouseItem.SetActive(true);
			mouseItem.GetComponent<MouseItem>().setItem(draggedItem);
			mouseItem.GetComponent<MouseItem>().itemIsFromEquipmentSlot = draggedEnum;		
		}
	}


    public void init(Character c)
    {
		myChar = c;
		c.OnSlotsChanged = updateSlots;
    }

    void updateSlots(Character c)
    {
		//Debug.Log ("Hallo update slots");

		updateEquipmentSlot(EquipmentSlots.BodyArmorSlot, c);
        updateEquipmentSlot(EquipmentSlots.HandLefSlot, c);
        updateEquipmentSlot(EquipmentSlots.HandRightSlot, c);
        updateEquipmentSlot(EquipmentSlots.HeadSlot, c);
        updateEquipmentSlot(EquipmentSlots.NecklaceSlot, c);
        updateEquipmentSlot(EquipmentSlots.PantsSlot, c);
        updateEquipmentSlot(EquipmentSlots.ShoeLeftSlot, c);
        updateEquipmentSlot(EquipmentSlots.ShoeRightSlot, c);
        updateEquipmentSlot(EquipmentSlots.TrinketSlot, c);

        for (int inventorySlot = 0; inventorySlot < 18; ++inventorySlot)
        {
            Item item = c.getInventoryItem(inventorySlot % 6, inventorySlot / 6);
            setSlotItem(BackPackSlots[inventorySlot], item);
        }

        for (int quickSlot = 0; quickSlot < 4; ++quickSlot)
        {
            Item item = c.getQuickSlotItem(quickSlot);
            setSlotItem(QuickSlots[quickSlot], item);
        }
    }

    void updateEquipmentSlot(EquipmentSlots slot, Character c) {
		//Debug.Log ("slot " + slot);
        Item item = c.getEquippedItem(slot);
        setSlotItem(ImageSlotDictionary[slot], item);
    }

    void setSlotItem(UnityEngine.UI.RawImage slot, Item item) {
		UnityEngine.UI.RawImage[] raws = slot.GetComponentsInChildren<UnityEngine.UI.RawImage>();

		if (item) {
			for (int i = 0; i < raws.Length ; ++i) {
				raws[i].enabled = true;
			}
            // children are rendered in front of their parents!
            slot.texture = BGImageCommon;
            slot.transform.GetChild(0).GetComponent<UnityEngine.UI.RawImage>().texture = item.ItemTexture;
        }
        else {
			for (int i = 0; i < raws.Length ; ++i) {
				raws[i].enabled = false;
			}

        }
    }
}
