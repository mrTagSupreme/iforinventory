﻿using UnityEngine;
using System.Collections;

public class PlayerGUIUpdater : MonoBehaviour {

    public UnityEngine.UI.Image HealthOrb, ManaOrb;
    public UnityEngine.UI.Text HealthText, ManaText;

    public UnityEngine.UI.Text GoldText;

    public UnityEngine.UI.Image XPBar;

    public UnityEngine.UI.Text StatsText;

    private Character Player;
    public void setPlayer(Character player)
    {
        Player = player;
        Player.Stats.OnBaseStatsChanged = StatsChanged;
        StatsChanged(Player.Stats);
    }


    void StatsChanged(BaseStats stats)
    {
        string modFormatString = "#;-#;0";
        string modPercentFormatString = "#%;-#%;0%";
        StatsText.text = "Level " + stats.Level.ToString() + "\n";
        StatsText.text += "Strength: " + stats.StrengthPoints.ToString(modFormatString) + " (+" + (stats.Strength - stats.StrengthPoints).ToString(modFormatString) + ")\n";
        StatsText.text += "Agility: " + stats.AgilityPoints.ToString(modFormatString) + " (+" + (stats.Agility - stats.AgilityPoints).ToString(modFormatString) + ")\n";
        StatsText.text += "Intelligence: " + stats.IntelligencePoints.ToString(modFormatString) + " (+" + (stats.Intelligence - stats.IntelligencePoints).ToString(modFormatString) + ")\n";
        StatsText.text += "Constitution: " + stats.ConstitutionPoints.ToString(modFormatString) + " (+" + (stats.Constitution - stats.ConstitutionPoints).ToString(modFormatString) + ")\n";
        StatsText.text += "Armor: " + stats.Armor.ToString(modFormatString) + "\n";
        StatsText.text += "Fire Resistance: " + stats.FireResistance.ToString(modFormatString) + "\n";
        StatsText.text += "Ice Resistance: " + stats.IceResistance.ToString(modFormatString) + "\n";
        StatsText.text += "Poison Resistance: " + stats.PoisonResistance.ToString(modFormatString) + "\n";
        StatsText.text += "Lightning Resistance: " + stats.LightningResistance.ToString(modFormatString) + "\n";
        StatsText.text += "Critical hit Chance: " + (stats.CriticalHitChance).ToString(modPercentFormatString) + "\n";
        StatsText.text += "Dodge Chance: " + (stats.DodgeChance).ToString(modPercentFormatString) + "\n";
        StatsText.text += "Block Chance: " + (stats.BlockChance).ToString(modPercentFormatString) + "\n";
    }

    // Update is called once per frame
    void Update() {
        if(Player){
            HealthOrb.fillAmount = Player.Stats.Health / Player.Stats.MaxHealth;
            HealthText.text = Player.Stats.Health.ToString("#;0") + " / " + Player.Stats.MaxHealth.ToString("#;0");
            ManaOrb.fillAmount = Player.Stats.Mana / Player.Stats.MaxMana;
            ManaText.text = Player.Stats.Mana.ToString("#;0") + " / " + Player.Stats.MaxMana.ToString("#;0");
            XPBar.fillAmount = Player.Stats.getXPProgress();
			GoldText.text = Player.Gold.ToString();
        }
	}
}
